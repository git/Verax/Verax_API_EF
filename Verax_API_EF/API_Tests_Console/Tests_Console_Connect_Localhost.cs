﻿// See https://aka.ms/new-console-template for more information

using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using API_Tests_Console;
using Entities;
using Model;

class Tests_Console_Connect_Localhost
{
    
    
    static readonly HttpClient client = new HttpClient();
    static string email = "verax@verax.com";
    static string password = "P@ssw0rd1";
    
    static async Task Main(string[] args)
    {
        //Commenter lignes apres premier execution
        await RegisterUser(email, password);
        
        
        string tokenAPI = await LoginAndGetToken(email, password);
        
        await TestUser(tokenAPI);
        await TestFormulaire(tokenAPI);
        await TestArticle(tokenAPI);
        
        
        //Pour executer les tests de la console en communiquant avec l'API deployée sur codeFirst
        //await Tests_Console_Connect_API.Main2(args);
    }

    private static async Task TestFormulaire(string tokenAPI)
    {
        await TestFormulaireGetAll(tokenAPI);
        await TestFormulaireGetId(tokenAPI);
        //await TestFormulaireCreate(tokenAPI);
        //await TestFormulaireDelete(tokenAPI);
        //await TestFormulaireUpdate(tokenAPI);
    }

    private static async Task TestUser(string tokenAPI)
    {
        await TestUserGetAll(tokenAPI);
        await TestUserGetId(tokenAPI);
        //await TestUserCreate(tokenAPI);
        //await TestUserDelete(tokenAPI);
        //await TestUserUpdate(tokenAPI);
        await TestGetAllArticleUser(tokenAPI);
        await TestGetArticleByUser(tokenAPI);
        //await TestCreateArticleUser(tokenAPI);
        //await TestDeleteArticleUser(tokenAPI);
        //await TestUpdateArticleUser(tokenAPI);
    }


    static async Task TestArticle(string tokenAPI)
    {
        await TestArticleGetId(tokenAPI);
        //await TestArticleCreate(tokenAPI);
        await TestArticleGetAll(tokenAPI);
        //await TestArticleDelete(tokenAPI);
        //await TestArticleUpdate(tokenAPI);
    }
    
    static async Task TestArticleGetAll(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/articles");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestArticleGetId(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/article/1");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestArticleCreate(string tokenAPI)
    {
        try
        {
            var article = new Article()
            {
                Title = "Test",
                Description = "Test",
                Author = "Test",
                DatePublished = "Test",
                LectureTime = 0
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(article);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("http://localhost:5052/article", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestArticleDelete(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.DeleteAsync("http://localhost:5052/article/1");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestArticleUpdate(string tokenAPI)
    {
        try
        {
            var article = new Article()
            {
                Title = "Louis",
                Description = "Je",
                Author = "T'",
                DatePublished = "aime",
                LectureTime = 0
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(article);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync("http://localhost:5052/article/1", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestFormulaireGetAll(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/formulaires");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestFormulaireGetId(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/formulaire/2");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestFormulaireCreate(string tokenAPI)
    {
        try
        {
            var formulaire = new Formulaire()
            {
                Theme = "Test",
                Date = "Test",
                Lien = "Test",
                UserPseudo = "Sha"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(formulaire);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("http://localhost:5052/formulaire", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestFormulaireDelete(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.DeleteAsync("http://localhost:5052/formulaire/5");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestFormulaireUpdate(string tokenAPI)
    {
        try
        {
            var formulaire = new Formulaire()
            {
                Theme = "J'",
                Date = "aime",
                Lien = "Les",
                UserPseudo = "Sha"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(formulaire);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync("http://localhost:5052/formulaire/4", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUserGetAll(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/users");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUserGetId(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/user/Sha");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUserCreate(string tokenAPI)
    {
        try
        {
            var user = new User()
            {
                Pseudo = "J",
                Nom = "'",
                Prenom = "aime",
                Mail = "les",
                Mdp = "pieds",
                Role = "Admin"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(user);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("http://localhost:5052/user", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUserDelete(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.DeleteAsync("http://localhost:5052/user/J");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUserUpdate(string tokenAPI)
    {
        try
        {
            var user = new User()
            {
                Pseudo = "Sha",
                Nom = "J'",
                Prenom = "aime",
                Mail = "les",
                Mdp = "pieds",
                Role = "Admin"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(user);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync("http://localhost:5052/user/Sha", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestGetAllArticleUser(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/user/article/users");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestGetArticleByUser(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.GetAsync("http://localhost:5052/user/Sha/articles");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestCreateArticleUser(string tokenAPI)
    {
        try
        {
            var articleUser = new ArticleUserEntity()
            {
                ArticleEntityId = 1,
                UserEntityPseudo = "Sha"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var json = JsonSerializer.Serialize(articleUser);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("http://localhost:5052/user/article", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    static async Task TestDeleteArticleUser(string tokenAPI)
    {
        try
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            var response = await client.DeleteAsync("http://localhost:5052/user/Sha/3");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    static async Task TestUpdateArticleUser(string tokenAPI)
    {
        try
        {
            var articleUser = new ArticleUserEntity()
            {
                ArticleEntityId = 2,
                UserEntityPseudo = "Sha"
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenAPI);
            long oldId = 3;
            var json = JsonSerializer.Serialize(articleUser);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync($"http://localhost:5052/user/Sha/{oldId}", data);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    
    public static async Task<string> LoginAndGetToken(string Email, string Password)
    {
        var credentials = new
        {
            email = Email,
            password = Password
        };

        var json = JsonSerializer.Serialize(credentials);
        var data = new StringContent(json, Encoding.UTF8, "application/json");

        try
        {
            var response = await client.PostAsync("http://localhost:5052/login", data);
            var responseBody = await response.Content.ReadAsStringAsync();
        
            if (response.IsSuccessStatusCode)
            {
                using (JsonDocument doc = JsonDocument.Parse(responseBody))
                {
                    if (doc.RootElement.TryGetProperty("accessToken", out JsonElement tokenElement)) // Change "token" to the actual property name
                    {
                        string token = tokenElement.GetString();
                        Console.WriteLine("Token retrieved");
                        return token;
                    }
                    else
                    {
                        Console.WriteLine("Token not found in the response.");
                        return null;
                    }
                }
            }
            else
            {
                Console.WriteLine($"Login failed: {response.StatusCode}");
                Console.WriteLine(responseBody);
                return null;
            }
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine($"HttpRequestException: {e.Message}");
            return null;
        }
        catch (JsonException e)
        {
            Console.WriteLine($"Error parsing JSON response: {e.Message}");
            return null;
        }
    }


    
   public static async Task<string> RegisterUser(string email, string password)
    {
        var newUser = new
        {
            email = email,
            password = password
        };

        var json = JsonSerializer.Serialize(newUser);
        var data = new StringContent(json, Encoding.UTF8, "application/json");

        try
        {
            var response = await client.PostAsync("http://localhost:5052/register", data);
            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Registration successful");
                Console.WriteLine(responseBody);
                return responseBody;
            }
            else
            {
                Console.WriteLine($"Registration failed: {response.StatusCode}");
                Console.WriteLine(responseBody);
                return null;
            }
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine($"HttpRequestException: {e.Message}");
            return null;
        }
        catch (Exception e)
        {
            Console.WriteLine($"General Exception: {e.Message}");
            return null;
        }
    }
}